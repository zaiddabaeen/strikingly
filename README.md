#Hangman (Android Application)
###By Zaid Daba'een for Strikingly


##How it works
Basically I have used [loopj's Android Async HTTP Client](http://loopj.com/android-async-http/) to send the JSON requests and to receive JSON responses.

All the REST APIs communications are thrown into `Rest.java` and there are functions for each basic operation that needs to be done which makes it easier later to edit and also to understand.

Calling API methods would be as easy as:

```java
    Rest.startGame(playerId, responseHandler);
```

`Game.java` is the Application class of the application which handles and saves global variables and methods.

`Response.java` contains static classes for each response of the APIs, an example for Start Game Response:

```java
	public static class StartGameResponse {

        public String message, sessionId;
        public int numberOfWordsToGuess, numberOfGuessAllowedForEachWord;

        public StartGameResponse(JSONObject response){

            try {
                message = response.getString("message");
                sessionId = response.getString("sessionId");
                JSONObject data = response.getJSONObject("data");
                numberOfWordsToGuess = data.getInt("numberOfWordsToGuess");
                numberOfGuessAllowedForEachWord = data.getInt("numberOfGuessAllowedForEachWord");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
```    

It handles the responses that are received and makes it easier to access them.

`MainActivityFragment.java` has all the gameplay methods. It first creates the keyboard layout. It creates a new game, saving the `sessionId` into the Application class `Game` and then requests a new word. All loading times are hidden behind a progress bar. The unknown letters of the words are underlined and spaced out. A little bit of fading is added to the keyboard.
The round ends when all the guesses were consumed, or when the user guesses the word.
The score can be shown anytime by clicking on "Score?". All information in the application is shown in a dialog for simplicity.

##Improvements
Due to the lack of personal time, and due to the fact that this is just a test task to show my logical skills, the following were not implemented, but would have been if this application would actually be deployed to the application stores:

1. There must be a cool design.
2. The XML layouts would have styles saved into the styles folder rather than setting them inline.
3. Unit testing would be created and ran.
4. A better structuring of the application files.
5. Icons would be added.
6. Eye candy everywhere, and sounds too.
7. Internet connectivity would be checked.
8. Error handling would be better.

Thank you,

7th of August 2015

Zaid Daba'een
