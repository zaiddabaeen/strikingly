package com.zaiddabaeen.strikingly.hangman;

import android.text.method.DateTimeKeyListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ZaidDabaeen on 8/7/15.
 */
public class Response {

    public static class StartGameResponse {

        public String message, sessionId;
        public int numberOfWordsToGuess, numberOfGuessAllowedForEachWord;

        public StartGameResponse(JSONObject response){

            try {
                message = response.getString("message");
                sessionId = response.getString("sessionId");
                JSONObject data = response.getJSONObject("data");
                numberOfWordsToGuess = data.getInt("numberOfWordsToGuess");
                numberOfGuessAllowedForEachWord = data.getInt("numberOfGuessAllowedForEachWord");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public static class WordResponse {

        public String sessionId, word;
        public int totalWordCount, wrongGuessCountOfCurrentWord;

        public WordResponse(JSONObject response){

            try {
                sessionId = response.getString("sessionId");
                JSONObject data = response.getJSONObject("data");
                word = data.getString("word");
                totalWordCount = data.getInt("totalWordCount");
                wrongGuessCountOfCurrentWord = data.getInt("wrongGuessCountOfCurrentWord");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public static class GetResultResponse {

        public String sessionId;
        public int totalWordCount, correctWordCount, totalWrongGuessCount, score;

        public GetResultResponse(JSONObject response){

            try {
                sessionId = response.getString("sessionId");
                JSONObject data = response.getJSONObject("data");
                totalWordCount = data.getInt("totalWordCount");
                correctWordCount = data.getInt("correctWordCount");
                totalWrongGuessCount = data.getInt("totalWrongGuessCount");
                score = data.getInt("score");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    public static class SubmitResultResponse {

        public String sessionId, message, playerId;
        public int totalWordCount, correctWordCount, totalWrongGuessCount, score;
        public Date datetime;

        public SubmitResultResponse(JSONObject response){

            try {
                sessionId = response.getString("sessionId");
                message = response.getString("message");
                JSONObject data = response.getJSONObject("data");
                playerId = data.getString("playerId");
                totalWordCount = data.getInt("totalWordCount");
                correctWordCount = data.getInt("correctWordCount");
                totalWrongGuessCount = data.getInt("totalWrongGuessCount");
                score = data.getInt("score");

                String sDatetime = data.getString("datetime");
                try {
                    datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sDatetime); //2014-10-28 11:45:58
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public static String getError(String response){
        try {
            JSONObject j = new JSONObject(response);
            return j.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "Not available";
    }

}
