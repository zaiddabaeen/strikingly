package com.zaiddabaeen.strikingly.hangman;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

public class MainActivity extends ActionBarActivity {

    ProgressDialog progressDialog;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
    }

    public void scoreClick(View v){
        showResults();
    }

    public void showResults() {

        progressDialog = ProgressDialog.show(context, "Loading", "Getting your results");

        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Log.i("response", statusCode + ": " + response.toString());

                final Response.GetResultResponse resultResponse = new Response.GetResultResponse(response);

                String message = "Score: " + resultResponse.score + "\n";
                message += "Correct word count: " + resultResponse.correctWordCount + "\n";
                message += "Total word count: " + resultResponse.totalWordCount + "\n";
                message += "Total wrong guesses: " + resultResponse.totalWrongGuessCount + "\n";

                progressDialog.dismiss();

                new AlertDialog.Builder(context)
                        .setTitle("Your results")
                        .setMessage(message)
                        .setNeutralButton("Okay", null)
                        .show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressDialog.dismiss();
                Game.showMessage("Error: " + Response.getError(responseString));

                Log.i("response", statusCode + ": " + responseString);
            }

        };

        Rest.getResult(Game.sessionId, responseHandler);

    }

    public void finishGame() {

        progressDialog = ProgressDialog.show(context, "Loading", "Submitting your results");

        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Log.i("response", statusCode + ": " + response.toString());

                final Response.SubmitResultResponse resultResponse = new Response.SubmitResultResponse(response);

                String message = "Player: " + resultResponse.playerId + "\n";
                message += "Correct word count: " + resultResponse.correctWordCount + "\n";
                message += "Total word count: " + resultResponse.totalWordCount + "\n";
                message += "Total wrong guesses: " + resultResponse.totalWrongGuessCount + "\n";
                message += "Score: " + resultResponse.score + "\n";

                progressDialog.dismiss();

                new AlertDialog.Builder(context)
                        .setTitle("Your results")
                        .setMessage(message)
                        .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setCancelable(false)
                        .show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressDialog.dismiss();
                Game.showMessage("Error: " + Response.getError(responseString));

                finish();
                Log.i("response", statusCode + ": " + responseString);
            }

        };

        Rest.submitResult(Game.sessionId, responseHandler);

    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(context)
                .setTitle("Exit")
                .setMessage("Would you like to save your results?")
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishGame();
                    }
                })
                .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();

    }
}
