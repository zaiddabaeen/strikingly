package com.zaiddabaeen.strikingly.hangman;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by ZaidDabaeen on 8/7/15.
 */
public class Rest {
    private static final String BASE_URL = "https://strikingly-hangman.herokuapp.com/game/on";

    public static final String START_GAME = "startGame";
    public static final String NEXT_WORD = "nextWord";
    public static final String GUESS_WORD = "guessWord";
    public static final String GET_RESULT = "getResult";
    public static final String SUBMIT_RESULT = "submitResult";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(BASE_URL, params, responseHandler);
    }

    public static void post(JSONObject jsonParams, AsyncHttpResponseHandler responseHandler) {
        try {
            StringEntity entity = new StringEntity(jsonParams.toString());
            client.post(Game.context, BASE_URL, entity, "application/json",
                    responseHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void startGame(String playerId, AsyncHttpResponseHandler responseHandler) {

        post(getStartGameJSON(playerId), responseHandler);

    }

    public static void nextWord(String sessionId, AsyncHttpResponseHandler responseHandler) {

        post(getNextWordJSON(sessionId), responseHandler);

    }

    public static void guessWord(String sessionId, String guess, AsyncHttpResponseHandler responseHandler) {

        post(getGuessWordJSON(sessionId, guess), responseHandler);

    }

    public static void getResult(String sessionId, AsyncHttpResponseHandler responseHandler) {

        post(getGetResultJSON(sessionId), responseHandler);

    }

    public static void submitResult(String sessionId, AsyncHttpResponseHandler responseHandler) {

        post(getSubmitResultJSON(sessionId), responseHandler);

    }

    private static JSONObject getStartGameJSON(String playerId) {
        try {
            JSONObject j = new JSONObject();
            j.put("playerId", playerId);
            j.put("action", START_GAME);

            return j;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static JSONObject getNextWordJSON(String sessionId) {
        try {
            JSONObject j = new JSONObject();
            j.put("sessionId", sessionId);
            j.put("action", NEXT_WORD);

            return j;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static JSONObject getGuessWordJSON(String sessionId, String guess) {
        try {
            JSONObject j = new JSONObject();
            j.put("sessionId", sessionId);
            j.put("action", GUESS_WORD);
            j.put("guess", guess);

            return j;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static JSONObject getGetResultJSON(String sessionId) {
        try {
            JSONObject j = new JSONObject();
            j.put("sessionId", sessionId);
            j.put("action", GET_RESULT);

            return j;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static JSONObject getSubmitResultJSON(String sessionId) {
        try {
            JSONObject j = new JSONObject();
            j.put("sessionId", sessionId);
            j.put("action", SUBMIT_RESULT);

            return j;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

}
