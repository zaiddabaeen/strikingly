package com.zaiddabaeen.strikingly.hangman;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import com.zaiddabaeen.strikingly.hangman.Response.*;

import java.util.ArrayList;

public class MainActivityFragment extends Fragment {

    Context context;
    TextView tWord, tLevel, tGuesses;
    TableLayout tblKeys;

    ProgressDialog progressDialog;

    ArrayList<Button> keys = new ArrayList<Button>();
    Boolean started = false;

    public MainActivityFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tWord = (TextView) view.findViewById(R.id.tWord);
        tLevel = (TextView) view.findViewById(R.id.tLevel);
        tGuesses = (TextView) view.findViewById(R.id.tGuesses);
        tblKeys = (TableLayout) view.findViewById(R.id.tblKeys);

        createKeyboardLayout();

    }

    private void createKeyboardLayout(){

        char allowedCharacters[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        float density = getResources().getDisplayMetrics().density;
        int w  = (int) (getResources().getDisplayMetrics().widthPixels - (32*density)) ;
        int buttonSize = 40;
        int buttonSizePx = (int) (buttonSize * density);
        int nButtonsCol = w / buttonSizePx;
        float nButtonsRowF = 26 / (float) nButtonsCol;
        int nButtonRow = (int) Math.ceil(nButtonsRowF);

        ArrayList<TableRow> tableRows = new ArrayList<TableRow>();

        for(int i = 0; i < nButtonRow; i++){
            TableRow tRow = new TableRow(context);
            tRow.setMotionEventSplittingEnabled(false);
            tableRows.add(tRow);
            tblKeys.addView(tRow);
        }

        int currentRow = 0;
        for(int i = 0 ; i < allowedCharacters.length ; i ++){
            currentRow = i / nButtonsCol;
            Button b = new Button(context);
            b.setText(allowedCharacters[i] + "");
            TableRow.LayoutParams params = new TableRow.LayoutParams();
            params.width = buttonSizePx;
            params.height =  buttonSizePx;
            b.setLayoutParams(params);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submitGuess((Button) v);
                }
            });
            keys.add(b);
            tableRows.get(currentRow).addView(b);
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if(!started){

            progressDialog = ProgressDialog.show(context, "Welcome", "Starting a new game");

            startGame(false);

            super.onStart();
        }
    }

    private void startGame(final boolean silent){

        started = true;

        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                StartGameResponse startGameResponse = new StartGameResponse(response);
                Game.sessionId = startGameResponse.sessionId;
                Game.numberOfWordsToGuess = startGameResponse.numberOfWordsToGuess;
                Game.numberOfGuessAllowedForEachWord = startGameResponse.numberOfGuessAllowedForEachWord;

                if(!silent) {
                    progressDialog.dismiss();
                    progressDialog = ProgressDialog.show(context, "Loading", "Getting you a new word");
                }

                nextWord(silent);
                Log.i("response", statusCode + ": " + response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Game.showMessage("Error: " + Response.getError(responseString));
                started = false;

                Log.i("response", statusCode + ": " + responseString);
            }

        };

        Rest.startGame(Game.playerId, responseHandler);

    }

    private void nextWord(final boolean silent){

        if(Game.totalWordCount > Game.numberOfWordsToGuess) {
            ((MainActivity) getActivity()).finishGame();
            startGame(true);
            return;
        }

        for(Button b : keys){
            b.animate().cancel();
            b.setAlpha(1);
            b.setEnabled(true);
            b.setClickable(true);
        }

        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                WordResponse wordResponse = new WordResponse(response);
                setWord(wordResponse.word);
                Game.totalWordCount = wordResponse.totalWordCount;
                tLevel.setText("Level: " + wordResponse.totalWordCount + "/" + Game.numberOfWordsToGuess);
                tGuesses.setText(Game.numberOfGuessAllowedForEachWord + "");

                if(!silent) progressDialog.dismiss();

                Log.i("response", statusCode + ": " + response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Game.showMessage("Error: " + Response.getError(responseString));

                tryNextWordAgain();

                Log.i("response", statusCode + ": " + responseString);
            }

        };

        Rest.nextWord(Game.sessionId, responseHandler);

    }

    private void submitGuess(final Button v) {

        final String guess = v.getText().toString();

        if(guess.length() != 1){
            Game.showMessage("Guess better");
            return;
        }

        for(Button b : keys){
            b.setClickable(false);
        }

        JsonHttpResponseHandler responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                WordResponse wordResponse = new WordResponse(response);
                setWord(wordResponse.word);
                tLevel.setText("Level: " + wordResponse.totalWordCount + "/" + Game.numberOfWordsToGuess);
                tGuesses.setText((Game.numberOfGuessAllowedForEachWord - wordResponse.wrongGuessCountOfCurrentWord) + "");

                v.animate().alpha(0).setDuration(300).start();
                v.setEnabled(false);

                for(Button b : keys){
                    b.setClickable(true);
                }
                Log.i("response", statusCode + ": " + response.toString());

                if(Game.lostRound(wordResponse)){
                    Game.showMessage("You lost this round");
                    nextWord(false);
                }
                if(Game.wonRound(wordResponse)){
                    nextWord(false);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Game.showMessage("Error: " + Response.getError(responseString));

                for(Button b : keys){
                    b.setClickable(true);
                }
                Log.i("response", statusCode + ": " + responseString);
            }

        };

        Rest.guessWord(Game.sessionId, guess, responseHandler);
    }

    private void tryNextWordAgain(){
        new AlertDialog.Builder(context)
                .setTitle("Try again")
                .setMessage("Try to get the next word again?")
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        nextWord(false);
                    }
                })
                .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                })
                .show();
    }

    private void setWord(String word){
        String newWord = "";
        char[] chars = word.toCharArray();
        for(char ch : chars){
            if(ch == '*') ch = '-';
            newWord += ch + " ";
        }
        newWord = newWord.trim();
        SpannableString content = new SpannableString(newWord);
        for(int i = 0 ; i < newWord.length() ; i += 2) {
            if(newWord.charAt(i) == '-') content.setSpan(new UnderlineSpan(), i, i+1, 0);
        }
        tWord.setText(content);
    }

}
