package com.zaiddabaeen.strikingly.hangman;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by ZaidDabaeen on 8/7/15.
 */
public class Game extends Application {

    public static final String playerId = "zaiddabaeen@hotmail.com";
    public static String sessionId;
    public static int numberOfWordsToGuess, numberOfGuessAllowedForEachWord, totalWordCount = 0;

    public static Context context;

    @Override
    public void onCreate() {

        context = this;

        super.onCreate();

    }

    public static void showMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean lostRound(Response.WordResponse wordResponse){
        return wordResponse.wrongGuessCountOfCurrentWord == numberOfGuessAllowedForEachWord;
    }

    public static boolean wonRound(Response.WordResponse wordResponse){
        return !wordResponse.word.contains("*");
    }
}
